#ifndef ROBOT_CMD_H
#define ROBOT_CMD_H

#pragma pack(1)
typedef struct{
    float pitch;
    float yaw;
    shoot_mode_e shoot_mode;
    friction_mode_e friction_mode;
    gimbal_mode_e gimbal_mode;
    loader_mode_e load_mode;
    Robot_Status_e robot_status;
}CANCommData_s;
#pragma pack()

/**
 * @brief 机器人核心控制任务初始化,会被RobotInit()调用
 * 
 */
void RobotCMDInit();

/**
 * @brief 机器人核心控制任务,200Hz频率运行(必须高于视觉发送频率)
 * 
 */
void RobotCMDTask();

#endif // !ROBOT_CMD_H