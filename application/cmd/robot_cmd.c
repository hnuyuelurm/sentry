// app
#include "robot_def.h"
#include "robot_cmd.h"
// module
#include "remote_control.h"
#include "ins_task.h"
#include "master_process.h"
#include "message_center.h"
#include "general_def.h"
#include "dji_motor.h"
#include "bmi088.h"
// bsp
#include "bsp_dwt.h"
#include "bsp_log.h"

// 私有宏,自动将编码器转换成角度值
#define YAW_ALIGN_ANGLE 128.539261f                                  // 对齐时的角度,0-360
#define PTICH_HORIZON_ANGLE (PITCH_HORIZON_ECD * ECD_ANGLE_COEF_DJI) // pitch水平时电机的角度,0-360

/* cmd应用包含的模块实例指针和交互信息存储*/
#if defined(GIMBAL_BOARD) || defined(MAIN_BOARD) // 对双板的兼容,条件编译
#include "can_comm.h"
static CANCommInstance *cmd_can_comm_left;  // 双板通信
static CANCommData_s *cmd_can_recv_left;    // 双板通信数据
static CANCommData_s cmd_can_send_left;     // 双板通信数据
static CANCommInstance *cmd_can_comm_right; // 双板通信
static CANCommData_s *cmd_can_recv_right;   // 双板通信数据
static CANCommData_s cmd_can_send_right;    // 双板通信数据
static uint16_t dwt_left;
static uint16_t dwt_right;

static uint32_t vision_send_flag = 0;
#endif

#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT) // 对双板的兼容,条件编译
#include "can_comm.h"
static CANCommInstance *cmd_can_comm; // 通信实例
static CANCommData_s *cmd_can_recv;   // 通信接收数据
static CANCommData_s cmd_can_send;    // 通信发送数据
static float yaw_aim_aps = 0.25f;
static float pitch_aim_aps = 0.25f;
#endif
#if defined(ONE_BOARD) || defined(MAIN_BOARD)
static Publisher_t *chassis_cmd_pub;   // 底盘控制消息发布者
static Subscriber_t *chassis_feed_sub; // 底盘反馈信息订阅者
#endif                                 // ONE_BOARD

#ifdef GIMBAL_BOARD_LEFT
#define YAW_MAX_ANGLE 215.0f
#define YAW_MIN_ANGLE -50.0f
#endif // GIMBAL_BOARD_LEFT

#ifdef GIMBAL_BOARD_RIGHT
#define YAW_MAX_ANGLE 50.0f
#define YAW_MIN_ANGLE -215.0f
#endif // GIMBAL_BOARD_RIGHT

static Chassis_Ctrl_Cmd_s chassis_cmd_send;      // 发送给底盘应用的信息,包括控制信息和UI绘制相关
static Chassis_Upload_Data_s chassis_fetch_data; // 从底盘应用接收的反馈信息信息,底盘功率枪口热量与底盘运动状态等

static RC_ctrl_t *rc_data;              // 遥控器数据,初始化时返回
static Vision_Recv_s *vision_recv_data; // 视觉接收数据指针,初始化时返回
static Vision_Send_s vision_send_data;  // 视觉发送数据

static Publisher_t *gimbal_cmd_pub;            // 云台控制消息发布者
static Subscriber_t *gimbal_feed_sub;          // 云台反馈信息订阅者
static Gimbal_Ctrl_Cmd_s gimbal_cmd_send;      // 传递给云台的控制信息
static Gimbal_Upload_Data_s gimbal_fetch_data; // 从云台获取的反馈信息

static Publisher_t *shoot_cmd_pub;           // 发射控制消息发布者
static Subscriber_t *shoot_feed_sub;         // 发射反馈信息订阅者
static Shoot_Ctrl_Cmd_s shoot_cmd_send;      // 传递给发射的控制信息
static Shoot_Upload_Data_s shoot_fetch_data; // 从发射获取的反馈信息

static Robot_Status_e robot_state; // 机器人整体工作状态

BMI088Instance *bmi088_test; // 云台IMU
BMI088_Data_t bmi088_data;

void RobotCMDInit()
{
    // BMI088_Init_Config_s bmi088_config = {
    //     .cali_mode = BMI088_CALIBRATE_ONLINE_MODE,
    //     .work_mode = BMI088_BLOCK_TRIGGER_MODE,
    //     .spi_acc_config = {
    //         .spi_handle = &hspi1,
    //         .GPIOx = GPIOA,
    //         .cs_pin = GPIO_PIN_4,
    //         .spi_work_mode = SPI_DMA_MODE,
    //     },
    //     .acc_int_config = {
    //         .GPIOx = GPIOC,
    //         .GPIO_Pin = GPIO_PIN_4,
    //         .exti_mode = GPIO_EXTI_MODE_RISING,
    //     },
    //     .spi_gyro_config = {
    //         .spi_handle = &hspi1,
    //         .GPIOx = GPIOB,
    //         .cs_pin = GPIO_PIN_0,
    //         .spi_work_mode = SPI_DMA_MODE,
    //     },
    //     .gyro_int_config = {
    //         .GPIO_Pin = GPIO_PIN_5,
    //         .GPIOx = GPIOC,
    //         .exti_mode = GPIO_EXTI_MODE_RISING,
    //     },
    //     .heat_pwm_config = {
    //         .htim = &htim10,
    //         .channel = TIM_CHANNEL_1,
    //         .period = 1,
    //     },
    //     .heat_pid_config = {
    //         .Kp = 0.5,
    //         .Ki = 0,
    //         .Kd = 0,
    //         .DeadBand = 0.1,
    //         .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
    //         .IntegralLimit = 100,
    //         .MaxOut = 100,
    //     },
    // };
    // bmi088_test = BMI088Register(&bmi088_config);

    gimbal_cmd_pub = PubRegister("gimbal_cmd", sizeof(Gimbal_Ctrl_Cmd_s));
    gimbal_feed_sub = SubRegister("gimbal_feed", sizeof(Gimbal_Upload_Data_s));
    shoot_cmd_pub = PubRegister("shoot_cmd", sizeof(Shoot_Ctrl_Cmd_s));
    shoot_feed_sub = SubRegister("shoot_feed", sizeof(Shoot_Upload_Data_s));

#ifdef ONE_BOARD // 双板兼容
    chassis_cmd_pub = PubRegister("chassis_cmd", sizeof(Chassis_Ctrl_Cmd_s));
    chassis_feed_sub = SubRegister("chassis_feed", sizeof(Chassis_Upload_Data_s));
    rc_data = RemoteControlInit(&huart3);   // 修改为对应串口,注意如果是自研板dbus协议串口需选用添加了反相器的那个
    vision_recv_data = VisionInit(&huart1); // 视觉通信串口
#endif                                      // ONE_BOARD
#ifdef GIMBAL_BOARD
    CANComm_Init_Config_s comm_conf = {
        .can_config = {
            .can_handle = &hcan2,
            .tx_id = 0x312,
            .rx_id = 0x311,
        },
        .recv_data_len = sizeof(Chassis_Upload_Data_s),
        .send_data_len = sizeof(Chassis_Ctrl_Cmd_s),
    };
    cmd_can_comm = CANCommInit(&comm_conf);
#endif // GIMBAL_BOARD
#ifdef MAIN_BOARD
    CANComm_Init_Config_s can_comm_conf = {
        .can_config = {
            .can_handle = &hcan2,
            .tx_id = 0x311,
            .rx_id = 0x312,
        },
        .recv_data_len = sizeof(CANCommData_s),
        .send_data_len = sizeof(CANCommData_s),
    };
    cmd_can_comm_left = CANCommInit(&can_comm_conf);
    can_comm_conf.can_config.tx_id = 0x313;
    can_comm_conf.can_config.rx_id = 0x314;
    cmd_can_comm_right = CANCommInit(&can_comm_conf);
    chassis_cmd_pub = PubRegister("chassis_cmd", sizeof(Chassis_Ctrl_Cmd_s));
    chassis_feed_sub = SubRegister("chassis_feed", sizeof(Chassis_Upload_Data_s));
    rc_data = RemoteControlInit(&huart3);   // 修改为对应串口,注意如果是自研板dbus协议串口需选用添加了反相器的那个
    vision_recv_data = VisionInit(&huart1); // 视觉通信串口

    robot_state = ROBOT_READY; // 启动时机器人进入工作模式,后续加入所有应用初始化完成之后再进入
#endif

#if defined(GIMBAL_BOARD_LEFT)
    CANComm_Init_Config_s can_comm_conf = {
        .can_config = {
            .can_handle = &hcan2,
            .tx_id = 0x312,
            .rx_id = 0x311,
        },
        .recv_data_len = sizeof(CANCommData_s),
        .send_data_len = sizeof(CANCommData_s),
    };
    cmd_can_comm = CANCommInit(&can_comm_conf);

    robot_state = ROBOT_STOP; // 从板初始化完成进入ROBOT_STOP模式，收到主板信号后再启动
#endif
#if defined(GIMBAL_BOARD_RIGHT)
    CANComm_Init_Config_s can_comm_conf = {
        .can_config = {
            .can_handle = &hcan2,
            .tx_id = 0x314,
            .rx_id = 0x313,
        },
        .recv_data_len = sizeof(CANCommData_s),
        .send_data_len = sizeof(CANCommData_s),
    };
    cmd_can_comm = CANCommInit(&can_comm_conf);
    robot_state = ROBOT_STOP; // 从板初始化完成进入ROBOT_STOP模式，收到主板信号后再启动
#endif
    gimbal_cmd_send.pitch = 0;
}

/**
 * @brief 根据gimbal app传回的当前电机角度计算和零位的误差
 *        单圈绝对角度的范围是0~360,说明文档中有图示
 *
 */
static void CalcOffsetAngle()
{
    // 别名angle提高可读性,不然太长了不好看,虽然基本不会动这个函数
    static float angle;
    angle = gimbal_fetch_data.yaw_motor_single_round_angle; // 从云台获取的当前yaw电机单圈角度
    // chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE;                  // 云台和底盘的夹角
#if YAW_ECD_GREATER_THAN_4096 // 如果大于180度
    if (angle > YAW_ALIGN_ANGLE && angle <= 180.0f + YAW_ALIGN_ANGLE)
        chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE;
    else if (angle > 180.0f + YAW_ALIGN_ANGLE)
        chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE - 360.0f;
    else
        chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE;
#else // 小于180度
    if (angle > YAW_ALIGN_ANGLE)
        chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE;
    else
        chassis_cmd_send.offset_angle = angle - YAW_ALIGN_ANGLE + 360.0f;
    if (chassis_cmd_send.offset_angle > 180.0f)
        chassis_cmd_send.offset_angle -= 360.0f;
#endif
}

/**
 * @brief 控制输入为遥控器(调试时)的模式和控制量设置
 *
 */
static void RemoteControlSet()
{
    // 控制底盘和云台运行模式,云台待添加,云台是否始终使用IMU数据?
    if (switch_is_down(rc_data[TEMP].rc.switch_right)) // 右侧开关状态[下],底盘跟随云台
    {
        chassis_cmd_send.chassis_mode = CHASSIS_NO_FOLLOW;
        gimbal_cmd_send.gimbal_mode = GIMBAL_GYRO_MODE;
    }
    else if (switch_is_mid(rc_data[TEMP].rc.switch_right)) // 右侧开关状态[中],底盘和云台分离,底盘保持不转动
    {
        chassis_cmd_send.chassis_mode = CHASSIS_FOLLOW_GIMBAL_YAW;
        gimbal_cmd_send.gimbal_mode = GIMBAL_FREE_MODE;
    }

    // 云台参数,确定云台控制数据
    if (switch_is_mid(rc_data[TEMP].rc.switch_left) || switch_is_up(rc_data[TEMP].rc.switch_left)) // 左侧开关状态为[中],视觉模式
    {
#ifdef MAIN_BOARD
        gimbal_cmd_send.gimbal_mode = GIMBAL_GYRO_MODE;
        if (vision_recv_data->vel_x < 1000 && vision_recv_data->vel_y < 1000 && vision_recv_data->angle < 1000)
        {
            chassis_cmd_send.vx = vision_recv_data->vel_x * 1000 / RADIUS_WHEEL / PI2 * 360.0f * REDUCTION_RATIO_WHEEL;
            chassis_cmd_send.vy = vision_recv_data->vel_y * 1000 / RADIUS_WHEEL / PI2 * 360.0f * REDUCTION_RATIO_WHEEL;
            gimbal_cmd_send.yaw = 0.0f;
        }
        // if (switch_is_up(rc_data[TEMP].rc.switch_left))
        //     chassis_cmd_send.chassis_mode = CHASSIS_ROTATE;
        // if (switch_is_mid(rc_data[TEMP].rc.switch_left))
        //     chassis_cmd_send.chassis_mode = CHASSIS_FOLLOW_GIMBAL_YAW;
        // if(chassis_fetch_data.current_outpost_hp < 10.0f)
        //     chassis_cmd_send.chassis_mode = CHASSIS_ROTATE;
        // else
        //     chassis_cmd_send.chassis_mode = CHASSIS_FOLLOW_GIMBAL_YAW;
        chassis_cmd_send.chassis_mode = CHASSIS_ROTATE;
        if (vision_recv_data->gimbal_select < 0) // 左板
        {
            cmd_can_send_left.pitch = -1 * vision_recv_data->pitch;
            cmd_can_send_left.yaw = vision_recv_data->yaw;
            if (vision_recv_data->target_state == READY_TO_FIRE)
            {
                dwt_left = 0;
                cmd_can_send_left.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_left.load_mode = LOAD_BURSTFIRE;
                cmd_can_send_left.shoot_mode = SHOOT_ON;
                cmd_can_send_left.friction_mode = FRICTION_ON;
            }
            if (vision_recv_data->target_state == TARGET_CONVERGING)
            {
                dwt_left = 0;
                cmd_can_send_left.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_left.friction_mode = FRICTION_ON;
                cmd_can_send_left.load_mode = LOAD_STOP;
                cmd_can_send_left.shoot_mode = SHOOT_ON;
            }
            if (vision_recv_data->target_state == NO_TARGET)
            {
                dwt_left++;
                cmd_can_send_left.pitch = cmd_can_recv_left->pitch;
                cmd_can_send_left.yaw = cmd_can_recv_left->yaw;
                cmd_can_send_left.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_left.shoot_mode = SHOOT_ON;
                cmd_can_send_left.load_mode = LOAD_STOP;
                cmd_can_send_left.friction_mode = FRICTION_ON;
                if (dwt_left > 100)
                    cmd_can_send_left.gimbal_mode = GIMBAL_AIM_MODE;
            }
        }
        if (vision_recv_data->gimbal_select > 0)
        {
            cmd_can_send_right.pitch = -1 * vision_recv_data->pitch;
            cmd_can_send_right.yaw = vision_recv_data->yaw;

            if (vision_recv_data->target_state == READY_TO_FIRE)
            {
                dwt_right = 0;
                cmd_can_send_right.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_right.load_mode = LOAD_BURSTFIRE;
                cmd_can_send_right.shoot_mode = SHOOT_ON;
                cmd_can_send_right.friction_mode = FRICTION_ON;
            }
            if (vision_recv_data->target_state == TARGET_CONVERGING)
            {
                dwt_right = 0;
                cmd_can_send_right.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_right.friction_mode = FRICTION_ON;
                cmd_can_send_right.load_mode = LOAD_STOP;
                cmd_can_send_right.shoot_mode = SHOOT_ON;
            }
            if (vision_recv_data->target_state == NO_TARGET)
            {
                dwt_right++;
                cmd_can_send_right.pitch = cmd_can_recv_right->pitch;
                cmd_can_send_right.yaw = cmd_can_recv_right->yaw;
                cmd_can_send_right.gimbal_mode = GIMBAL_GYRO_MODE;
                cmd_can_send_right.shoot_mode = SHOOT_ON;
                cmd_can_send_right.load_mode = LOAD_STOP;
                cmd_can_send_right.friction_mode = FRICTION_ON;
                if (dwt_right > 60)
                    cmd_can_send_right.gimbal_mode = GIMBAL_AIM_MODE;
            }
        }
#endif

        // chassis_cmd_send.wz = vision_recv_data->angle * RAD_2_DEGREE;
    }
    // 左侧开关状态为[下],或视觉未识别到目标,纯遥控器拨杆控制
    if (switch_is_down(rc_data[TEMP].rc.switch_left))
    { // 按照摇杆的输出大小进行角度增量,增益系数需调整
        // if(gimbal_cmd_send)
        if (gimbal_cmd_send.gimbal_mode == GIMBAL_FREE_MODE)
        {
            gimbal_cmd_send.yaw += 0.002f * (float)rc_data[TEMP].rc.rocker_l_;
        }
        if (gimbal_cmd_send.gimbal_mode == GIMBAL_GYRO_MODE)
        {
            gimbal_cmd_send.yaw = 0.0f;
        }
        chassis_cmd_send.vx = 30.0f * (float)rc_data[TEMP].rc.rocker_r1;  // _水平方向
        chassis_cmd_send.vy = -30.0f * (float)rc_data[TEMP].rc.rocker_r_; // 1数值方向
        chassis_cmd_send.wz = 0;
#ifdef MAIN_BOARD
        cmd_can_send_left.gimbal_mode = GIMBAL_FREE_MODE;
        cmd_can_send_left.pitch = 0.001f * (float)rc_data[TEMP].rc.rocker_l1;
        cmd_can_send_left.yaw = 0.001f * (float)rc_data[TEMP].rc.rocker_l_;
        cmd_can_send_left.shoot_mode = shoot_cmd_send.shoot_mode;
        cmd_can_send_left.friction_mode = shoot_cmd_send.friction_mode;
        cmd_can_send_left.load_mode = shoot_cmd_send.load_mode;

        cmd_can_send_right.gimbal_mode = GIMBAL_FREE_MODE;
        cmd_can_send_right.pitch = 0.001f * (float)rc_data[TEMP].rc.rocker_l1;
        cmd_can_send_right.yaw = 0.001f * (float)rc_data[TEMP].rc.rocker_l_;
        cmd_can_send_right.shoot_mode = shoot_cmd_send.shoot_mode;
        cmd_can_send_right.friction_mode = shoot_cmd_send.friction_mode;
        cmd_can_send_right.load_mode = shoot_cmd_send.load_mode;
        // if (switch_is_up(rc_data[TEMP].rc.switch_right)) // 右侧开关状态[上],弹舱打开
        // chassis_cmd_send.wz = 500;
        //                                                 // 弹舱舵机控制,待添加servo_motor模块,开启
        // else
        // chassis_cmd_send.wz = 0;
        // 弹舱舵机控制,待添加servo_motor模块,关闭
#endif
        // chassis_cmd_send.wz = 5.0f * (float)rc_data[TEMP].rc.rocker_l_;  // 旋转
    }
    // 云台软件限位

    // 底盘参数,目前没有加入小陀螺(调试似乎暂时没有必要),系数需要调整

    // 发射参数

    // 摩擦轮控制,拨轮向上打为负,向下为正
    if (rc_data[TEMP].rc.dial < -100) // 向上超过100,打开摩擦轮
    {
        shoot_cmd_send.friction_mode = FRICTION_ON;
    }

    else
    {
        shoot_cmd_send.friction_mode = FRICTION_OFF;
    }
    // 拨弹控制,遥控器固定为一种拨弹模式,可自行选择
    if (rc_data[TEMP].rc.dial < -500)
        shoot_cmd_send.load_mode = LOAD_BURSTFIRE;
    else
    {
        shoot_cmd_send.load_mode = LOAD_STOP;
    }

    // 射频控制,固定每秒1发,后续可以根据左侧拨轮的值大小切换射频,
    shoot_cmd_send.shoot_rate = 12;
}

/**
 * @brief 输入为键鼠时模式和控制量设置
 *
 */
static void MouseKeySet()
{
    chassis_cmd_send.vx = rc_data[TEMP].key[KEY_PRESS].w * 300 - rc_data[TEMP].key[KEY_PRESS].s * 300; // 系数待测
    chassis_cmd_send.vy = rc_data[TEMP].key[KEY_PRESS].s * 300 - rc_data[TEMP].key[KEY_PRESS].d * 300;

    gimbal_cmd_send.yaw += (float)rc_data[TEMP].mouse.x / 660 * 10; // 系数待测
    gimbal_cmd_send.pitch += (float)rc_data[TEMP].mouse.y / 660 * 10;

    switch (rc_data[TEMP].key_count[KEY_PRESS][Key_Z] % 3) // Z键设置弹速
    {
    case 0:
        shoot_cmd_send.bullet_speed = 15;
        break;
    case 1:
        shoot_cmd_send.bullet_speed = 18;
        break;
    default:
        shoot_cmd_send.bullet_speed = 30;
        break;
    }
    switch (rc_data[TEMP].key_count[KEY_PRESS][Key_E] % 4) // E键设置发射模式
    {
    case 0:
        shoot_cmd_send.load_mode = LOAD_STOP;
        break;
    case 1:
        shoot_cmd_send.load_mode = LOAD_1_BULLET;
        break;
    case 2:
        shoot_cmd_send.load_mode = LOAD_3_BULLET;
        break;
    default:
        shoot_cmd_send.load_mode = LOAD_BURSTFIRE;
        break;
    }
    switch (rc_data[TEMP].key_count[KEY_PRESS][Key_R] % 2) // R键开关弹舱
    {
    case 0:
        shoot_cmd_send.lid_mode = LID_OPEN;
        break;
    default:
        shoot_cmd_send.lid_mode = LID_CLOSE;
        break;
    }
    switch (rc_data[TEMP].key_count[KEY_PRESS][Key_F] % 2) // F键开关摩擦轮
    {
    case 0:
        shoot_cmd_send.friction_mode = FRICTION_OFF;
        break;
    default:
        shoot_cmd_send.friction_mode = FRICTION_ON;
        break;
    }
    switch (rc_data[TEMP].key_count[KEY_PRESS][Key_C] % 4) // C键设置底盘速度
    {
    case 0:
        chassis_cmd_send.chassis_speed_buff = 40;
        break;
    case 1:
        chassis_cmd_send.chassis_speed_buff = 60;
        break;
    case 2:
        chassis_cmd_send.chassis_speed_buff = 80;
        break;
    default:
        chassis_cmd_send.chassis_speed_buff = 100;
        break;
    }
    switch (rc_data[TEMP].key[KEY_PRESS].shift) // 待添加 按shift允许超功率 消耗缓冲能量
    {
    case 1:

        break;

    default:

        break;
    }
}

/**
 * @brief  紧急停止,包括遥控器左上侧拨轮打满/重要模块离线/双板通信失效等
 *         停止的阈值'300'待修改成合适的值,或改为开关控制.
 *
 * @todo   后续修改为遥控器离线则电机停止(关闭遥控器急停),通过给遥控器模块添加daemon实现
 *
 */
static void EmergencyHandler()
{
#if defined(MAIN_BOARD) || defined(ONE_BOARD) || defined(GIMBAL_BOARD)
    // 拨轮的向下拨超过一半进入急停模式.注意向打时下拨轮是正
    if (rc_data[TEMP].rc.dial > 300 || robot_state == ROBOT_STOP) // 还需添加重要应用和模块离线的判断
    {
        robot_state = ROBOT_STOP;
        gimbal_cmd_send.gimbal_mode = GIMBAL_ZERO_FORCE;
        chassis_cmd_send.chassis_mode = CHASSIS_ZERO_FORCE;
        shoot_cmd_send.shoot_mode = SHOOT_OFF;
        shoot_cmd_send.friction_mode = FRICTION_OFF;
        shoot_cmd_send.load_mode = LOAD_STOP;
        LOGERROR("[CMD] emergency stop!");
    }
    // 遥控器右侧开关为[上],恢复正常运行
    if (switch_is_up(rc_data[TEMP].rc.switch_right))
    {
        robot_state = ROBOT_READY;
        shoot_cmd_send.shoot_mode = SHOOT_ON;
        LOGINFO("[CMD] reinstate, robot ready");
    }
#endif
#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT)
    if (robot_state == ROBOT_STOP) // 还需添加重要应用和模块离线的判断
    {
        robot_state = ROBOT_STOP;
        gimbal_cmd_send.gimbal_mode = GIMBAL_ZERO_FORCE;
        chassis_cmd_send.chassis_mode = CHASSIS_ZERO_FORCE;
        shoot_cmd_send.shoot_mode = SHOOT_OFF;
        shoot_cmd_send.friction_mode = FRICTION_OFF;
        shoot_cmd_send.load_mode = LOAD_STOP;
        LOGERROR("[CMD] emergency stop!");
    }
    // 遥控器右侧开关为[上],恢复正常运行
    else
    {
        shoot_cmd_send.shoot_mode = SHOOT_ON;
        LOGINFO("[CMD] reinstate, robot ready");
    }
#endif
}

/* 机器人核心控制任务,200Hz频率运行(必须高于视觉发送频率) */
void RobotCMDTask()
{
    // BMI088Acquire(bmi088_test,&bmi088_data) ;
    // 从其他应用获取回传数据
#ifdef ONE_BOARD
    SubGetMessage(chassis_feed_sub, (void *)&chassis_fetch_data);
#endif // ONE_BOARD
#ifdef GIMBAL_BOARD
    chassis_fetch_data = *(Chassis_Upload_Data_s *)CANCommGet(cmd_can_comm);
#endif // GIMBAL_BOARD
#ifdef MAIN_BOARD
    SubGetMessage(chassis_feed_sub, (void *)&chassis_fetch_data);
    cmd_can_recv_left = (CANCommData_s *)CANCommGet(cmd_can_comm_left);
    cmd_can_recv_right = (CANCommData_s *)CANCommGet(cmd_can_comm_right);
#endif // MAIN_BOARD
#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT)
    cmd_can_recv = (CANCommData_s *)CANCommGet(cmd_can_comm);
#endif
    SubGetMessage(shoot_feed_sub, &shoot_fetch_data);
    SubGetMessage(gimbal_feed_sub, &gimbal_fetch_data);

    // 根据gimbal的反馈值计算云台和底盘正方向的夹角,不需要传参,通过static私有变量完成
    CalcOffsetAngle();
#if defined(MAIN_BOARD) || defined(ONE_BOARD) || defined(GIMBAL_BOARD)
    // 根据遥控器左侧开关,确定当前使用的控制模式为遥控器调试还是键鼠
    if (switch_is_down(rc_data[TEMP].rc.switch_left) || switch_is_mid(rc_data[TEMP].rc.switch_left) || switch_is_up(rc_data[TEMP].rc.switch_left)) // 遥控器左侧开关状态为[下],遥控器控制
    {
        RemoteControlSet();
    }
    // else if (switch_is_up(rc_data[TEMP].rc.switch_left)) // 遥控器左侧开关状态为[上],键盘控制
    //     MouseKeySet();
#endif
#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT)
    gimbal_cmd_send.gimbal_mode = cmd_can_recv->gimbal_mode;
    if (gimbal_cmd_send.gimbal_mode == GIMBAL_GYRO_MODE)
    {
        gimbal_cmd_send.yaw = cmd_can_recv->yaw;
        gimbal_cmd_send.pitch = cmd_can_recv->pitch;
        if (gimbal_cmd_send.pitch > PITCH_MAX_ANGLE)
            gimbal_cmd_send.pitch = PITCH_MAX_ANGLE;
        if (gimbal_cmd_send.pitch < PITCH_MIN_ANGLE)
            gimbal_cmd_send.pitch = PITCH_MIN_ANGLE;
        if (gimbal_cmd_send.yaw > YAW_MAX_ANGLE)
            gimbal_cmd_send.yaw = YAW_MAX_ANGLE;
        if (gimbal_cmd_send.yaw < YAW_MIN_ANGLE)
            gimbal_cmd_send.yaw = YAW_MIN_ANGLE;
    }
    else if (gimbal_cmd_send.gimbal_mode == GIMBAL_FREE_MODE)
    {
        // gimbal_cmd_send.yaw = cmd_can_recv->yaw;
        gimbal_cmd_send.yaw += -0.5f * cmd_can_recv->yaw;
        // gimbal_cmd_send.yaw -= 0.0002;
        gimbal_cmd_send.pitch += cmd_can_recv->pitch;
        if (gimbal_cmd_send.pitch > PITCH_MAX_ANGLE)
            gimbal_cmd_send.pitch = PITCH_MAX_ANGLE;
        if (gimbal_cmd_send.pitch < PITCH_MIN_ANGLE)
            gimbal_cmd_send.pitch = PITCH_MIN_ANGLE;
        if (gimbal_cmd_send.yaw > YAW_MAX_ANGLE)
            gimbal_cmd_send.yaw = YAW_MAX_ANGLE;
        if (gimbal_cmd_send.yaw < YAW_MIN_ANGLE)
            gimbal_cmd_send.yaw = YAW_MIN_ANGLE;
    }
    else if (gimbal_cmd_send.gimbal_mode == GIMBAL_AIM_MODE)
    {
        if (gimbal_cmd_send.yaw > YAW_MAX_ANGLE)
            yaw_aim_aps = -0.15f;
        if (gimbal_cmd_send.yaw < YAW_MIN_ANGLE)
            yaw_aim_aps = 0.15f;
        if (gimbal_cmd_send.pitch > PITCH_MAX_ANGLE)
            pitch_aim_aps = -0.17f;
        if (gimbal_cmd_send.pitch < PITCH_MIN_ANGLE)
            pitch_aim_aps = 0.17f;

        gimbal_cmd_send.yaw += yaw_aim_aps;
        gimbal_cmd_send.pitch += pitch_aim_aps;
        // if (gimbal_cmd_send.pitch > PITCH_MAX_ANGLE)
        //     gimbal_cmd_send.pitch = PITCH_MAX_ANGLE;
        // if (gimbal_cmd_send.pitch < PITCH_MIN_ANGLE)
        //     gimbal_cmd_send.pitch = PITCH_MIN_ANGLE;
        // if (gimbal_cmd_send.yaw > YAW_MAX_ANGLE)
        //     gimbal_cmd_send.yaw = YAW_MAX_ANGLE;
        // if (gimbal_cmd_send.yaw < YAW_MIN_ANGLE)
        //     gimbal_cmd_send.yaw = YAW_MIN_ANGLE;
    }
    shoot_cmd_send.friction_mode = cmd_can_recv->friction_mode;
    shoot_cmd_send.shoot_mode = cmd_can_recv->shoot_mode;
    shoot_cmd_send.load_mode = cmd_can_recv->load_mode;
    shoot_cmd_send.shoot_rate = 15.0f;
    cmd_can_send.yaw = gimbal_fetch_data.gimbal_imu_data.YawTotalAngle;
    cmd_can_send.pitch = gimbal_fetch_data.gimbal_imu_data.Roll;
    robot_state = cmd_can_recv->robot_status;
#endif // GIMBAL_BOARD_LEFT || GIMBAL_BOARD_RIGHT
    // 设置视觉发送数据,还需增加加速度和角速度数据
    // VisionSetFlag(chassis_fetch_data.enemy_color,,chassis_fetch_data.bullet_speed)
    RefreeSetAltitude(chassis_fetch_data.current_HP, chassis_fetch_data.stage_remain_time, chassis_fetch_data.game_progress, chassis_fetch_data.current_enemy_sentry_hp, chassis_fetch_data.current_enemy_base_hp, chassis_fetch_data.current_enemy_outpost_hp, chassis_fetch_data.current_shield_hp, chassis_fetch_data.current_base_hp, chassis_fetch_data.allow_fire_amount, chassis_fetch_data.current_outpost_hp);
    // 推送消息,双板通信,视觉通信等
    // 其他应用所需的控制数据在remotecontrolsetmode和mousekeysetmode中完成设置
    VisionSetFlag(chassis_fetch_data.enemy_color, VISION_MODE_AIM, chassis_fetch_data.bullet_speed);
    // VisionSetAltitude(yaw, pitch, roll);

    // 发送数据的代码
    // chassis_fetch_data.real_vx / chassis_fetch_data.real_vy 单位：m/s，坐标系为云台系；云台正方向为y轴，云台右方向为x轴，满足右手系
    // gimbal_fetch_data.gimbal_imu_data.Yaw 单位：° ，-180~180
    SlamSetAltitude(chassis_fetch_data.real_vx, chassis_fetch_data.real_vy, gimbal_fetch_data.gimbal_imu_data.Gyro[2], chassis_fetch_data.cmd_enable, chassis_fetch_data.cmd_x, chassis_fetch_data.cmd_y);
    EmergencyHandler(); // 处理模块离线和遥控器急停等紧急情况
#ifdef ONE_BOARD
    PubPushMessage(chassis_cmd_pub, (void *)&chassis_cmd_send);
#endif // ONE_BOARD
#ifdef GIMBAL_BOARD
    CANCommSend(cmd_can_comm, (void *)&chassis_cmd_send);
#endif // GIMBAL_BOARD
#ifdef MAIN_BOARD
    PubPushMessage(chassis_cmd_pub, (void *)&chassis_cmd_send);
    cmd_can_send_left.robot_status = robot_state;
    cmd_can_send_right.robot_status = robot_state;
    if (chassis_fetch_data.rest_heat_l < 100)
    {
        cmd_can_send_left.load_mode = LOAD_STOP;
    }
    if (chassis_fetch_data.rest_heat_r < 100)
    {
        cmd_can_send_right.load_mode = LOAD_STOP;
    }
    uint8_t senddata[sizeof(CANCommData_s)];
    memcpy(senddata, &cmd_can_send_left, sizeof(CANCommData_s));
    CANCommSend(cmd_can_comm_left, senddata);
    memcpy(senddata, &cmd_can_send_right, sizeof(CANCommData_s));
    CANCommSend(cmd_can_comm_right, senddata);
    vision_send_flag++;
    if (vision_send_flag % 2)
    {
        VisionSetAltitude(cmd_can_recv_right->yaw, -cmd_can_recv_right->pitch, 0, 1.0f);
    }
    else
    {
        VisionSetAltitude(cmd_can_recv_left->yaw, -cmd_can_recv_left->pitch, 0, -1.0f);
    }

#endif // MAIN_BOARD
#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT)
    uint8_t senddata[sizeof(CANCommData_s)];
    memcpy(senddata, &cmd_can_send, sizeof(CANCommData_s));
    CANCommSend(cmd_can_comm, senddata);
#endif // GIMBAL_BOARD_LEFT || GIMBAL_BOARD_RIGHT
    PubPushMessage(shoot_cmd_pub, (void *)&shoot_cmd_send);
    PubPushMessage(gimbal_cmd_pub, (void *)&gimbal_cmd_send);
}
