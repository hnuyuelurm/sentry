#include "gimbal.h"
#include "robot_def.h"
#include "dji_motor.h"
#include "dmmotor.h"
#include "ins_task.h"
#include "message_center.h"
#include "general_def.h"
#include "bmi088.h"
#include "buzzer.h"
#include "encoder.h"

static attitude_t *gimba_IMU_data;  // 云台IMU数据
static attitude_t *gimba_IMU_data2; // 云台IMU数据
static DJIMotorInstance *yaw_motor, *pitch_motor;
static EncoderInstance *yaw_encoder;
static float yaw_angle_ff;
#ifdef MAIN_BOARD
static DMMotorInstance *chassis_yaw_motor; // 底盘大云台yaw
#endif
static BuzzzerInstance *buzzer;
static float yaw_angle_offset;
static Publisher_t *gimbal_pub;                   // 云台应用消息发布者(云台反馈给cmd)
static Subscriber_t *gimbal_sub;                  // cmd控制消息订阅者
static Gimbal_Upload_Data_s gimbal_feedback_data; // 回传给cmd的云台状态信息
static Gimbal_Ctrl_Cmd_s gimbal_cmd_recv;         // 来自cmd的控制信息
static float pitch,yaw;

static BMI088Instance *bmi088; // 云台IMU
void GimbalInit()
{
    BuzzerInit();
    Buzzer_config_s buzzer_config = {
        .alarm_level = ALARM_LEVEL_HIGH,
        .loudness = 0.5,
        .octave = 1,
    };
    buzzer = BuzzerRegister(&buzzer_config);
    gimba_IMU_data = INS_Init(&hspi1); // IMU先初始化,获取姿态数据指针赋给yaw电机的其他数据来源
    
    yaw = 0;
    pitch = 0;
    // EncoderConfig_s encoder_config = {
    //     .htim = &htim8,
    // };
    // yaw_encoder = EncoderRegister(&encoder_config);
    // gimba_IMU_data2 = INS_Init(&hspi2);

#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD) || defined(ONE_BOARD)
    //  YAW
    Motor_Init_Config_s yaw_config = {
        .can_init_config = {
            .can_handle = &hcan1,
            .tx_id = 6,
        },
        .controller_param_init_config = {
            .angle_PID = {
                .Kp = 0.38, // 8
                .Ki = 0,
                .Kd = 0,
                .DeadBand = 0.1,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 100,

                .MaxOut = 25000,
            },
            .speed_PID = {
                .Kp = 3000, // 50
                .Ki = 100,  // 200
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement | PID_OutputFilter,
                .IntegralLimit = 3000,
                .MaxOut = 20000,
                .CoefA = 0.7,
                .CoefB = 0.5,
                .Derivative_LPF_RC = 0.01,
            },
            .other_angle_feedback_ptr = &gimba_IMU_data->YawTotalAngle,
            // 还需要增加角速度额外反馈指针,注意方向,ins_task.md中有c板的bodyframe坐标系说明
            .other_speed_feedback_ptr = &gimba_IMU_data->Gyro[2],
        },
        .controller_setting_init_config = {
            .angle_feedback_source = OTHER_FEED,
            .speed_feedback_source = OTHER_FEED,
            .outer_loop_type = ANGLE_LOOP,
            .close_loop_type = ANGLE_LOOP | SPEED_LOOP,
            .motor_reverse_flag = MOTOR_DIRECTION_NORMAL,
        },
        .motor_type = GM6020};
    // PITCH
    Motor_Init_Config_s pitch_config = {
        .can_init_config = {
            .can_handle = &hcan1,
            .tx_id = 5,
        },
        .controller_param_init_config = {
            .angle_PID = {
                .Kp = 0.48, // 10
                .Ki = 0.5,
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 100,
                .MaxOut = 500,
                .CoefA = 0.05,
                .CoefB = 0.05,
            },
            .speed_PID = {
                .Kp = 7000, // 50
                .Ki = 0,    // 350
                .Kd = 0,    // 0
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 2500,
                .MaxOut = 8000,
            },
            .other_angle_feedback_ptr = &gimba_IMU_data->Roll,
            // 还需要增加角速度额外反馈指针,注意方向,ins_task.md中有c板的bodyframe坐标系说明
            .other_speed_feedback_ptr = (&gimba_IMU_data->Gyro[1]),
        },
        .controller_setting_init_config = {
            .angle_feedback_source = OTHER_FEED,
            .speed_feedback_source = OTHER_FEED,
            .outer_loop_type = ANGLE_LOOP,
            .close_loop_type = SPEED_LOOP | ANGLE_LOOP,
            .motor_reverse_flag = MOTOR_DIRECTION_NORMAL,
            .feedback_reverse_flag = FEEDBACK_DIRECTION_REVERSE,
        },
        .motor_type = GM6020,
    };
    // 电机对total_angle闭环,上电时为零,会保持静止,收到遥控器数据再动
    yaw_motor = DJIMotorInit(&yaw_config);
    pitch_motor = DJIMotorInit(&pitch_config);
#endif
#ifdef GIMBAL_BOARD_RIGHT
    Motor_Init_Config_s yaw_config = {
        .can_init_config = {
            .can_handle = &hcan1,
            .tx_id = 5,
        },
        .controller_param_init_config = {
            .angle_PID = {
                .Kp = 0.345, // 0.345
                .Ki = 0.03,// 0.045
                .Kd = 0,
                .DeadBand = 0.1,
                .CoefA = 0.5,
                .CoefB = 1.5,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement | PID_ChangingIntegrationRate,
                .IntegralLimit = 100,
                .MaxOut = 60,
            },
            .speed_PID = {
                .Kp = 2700, // 50
                .Ki = 100,  // 200
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement | PID_OutputFilter,
                .IntegralLimit = 3000,
                .MaxOut = 20000,
                .CoefA = 0.7,
                .CoefB = 0.5,
                .Output_LPF_RC = 0.01,
            },
            .other_angle_feedback_ptr = &gimba_IMU_data->YawTotalAngle,
            // 还需要增加角速度额外反馈指针,注意方向,ins_task.md中有c板的bodyframe坐标系说明
            .other_speed_feedback_ptr = &gimba_IMU_data->Gyro[2],
            .current_feedforward_ptr = &yaw_angle_ff,
        },
        .controller_setting_init_config = {
            .angle_feedback_source = OTHER_FEED,
            .speed_feedback_source = OTHER_FEED,
            .outer_loop_type = ANGLE_LOOP,
            .close_loop_type = ANGLE_LOOP | SPEED_LOOP,
            .motor_reverse_flag = MOTOR_DIRECTION_NORMAL,

        },
        .motor_type = GM6020};
    // PITCH
    Motor_Init_Config_s pitch_config = {
        .can_init_config = {
            .can_handle = &hcan1,
            .tx_id = 6,
        },
        .controller_param_init_config = {
            .angle_PID = {
                .Kp = 0.6, // 10
                .Ki = 0.005,
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement | PID_ChangingIntegrationRate,
                .IntegralLimit = 100,
                .MaxOut = 60,
                .CoefA = 0.05,
                .CoefB = 0.75,
            },
            .speed_PID = {
                .Kp = 5800, // 50
                .Ki = 0,    // 350
                .Kd = 0,    // 0
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 2500,
                .MaxOut = 8000,
            },
            .other_angle_feedback_ptr = &gimba_IMU_data->Roll,
            // 还需要增加角速度额外反馈指针,注意方向,ins_task.md中有c板的bodyframe坐标系说明
            .other_speed_feedback_ptr = (&gimba_IMU_data->Gyro[1]),
        },
        .controller_setting_init_config = {
            .angle_feedback_source = OTHER_FEED,
            .speed_feedback_source = OTHER_FEED,
            .outer_loop_type = ANGLE_LOOP,
            .close_loop_type = SPEED_LOOP | ANGLE_LOOP,
            .motor_reverse_flag = MOTOR_DIRECTION_NORMAL,
        },
        .motor_type = GM6020,
    };
    // 电机对total_angle闭环,上电时为零,会保持静止,收到遥控器数据再动
    yaw_motor = DJIMotorInit(&yaw_config);
    pitch_motor = DJIMotorInit(&pitch_config);
#endif
#ifdef MAIN_BOARD
    Motor_Init_Config_s chassis_yaw_config = {
        .can_init_config = {
            .can_handle = &hcan1,
            .tx_id = 0x01,
        },
        .controller_param_init_config = {
            .angle_PID = {
                .Kp = 0.16, // 8
                .Ki = 0,
                .Kd = 0.005,
                .DeadBand = 0.1,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 100,

                .MaxOut = 250,
            },
            .speed_PID = {
                .Kp = 3.8, // 0.742
                .Ki = 0,
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 30,
                .MaxOut = 10,
            },
            .torque_PID = {
                .Kp = 0, // 0.742
                .Ki = 0, // 0
                .Kd = 0,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .IntegralLimit = 3,
                .MaxOut = 15,
            },
            .other_angle_feedback_ptr = &gimba_IMU_data->YawTotalAngle,
            .other_speed_feedback_ptr = &gimba_IMU_data->Gyro[2],

        },
        .controller_setting_init_config = {
            .angle_feedback_source = OTHER_FEED,
            .speed_feedback_source = OTHER_FEED,
            .outer_loop_type = ANGLE_LOOP,
            .close_loop_type = ANGLE_LOOP | SPEED_LOOP,
            .motor_reverse_flag = MOTOR_DIRECTION_NORMAL,

        },
    };
    chassis_yaw_motor = DMMotorInit(&chassis_yaw_config);
#endif
    gimbal_pub = PubRegister("gimbal_feed", sizeof(Gimbal_Upload_Data_s));
    gimbal_sub = SubRegister("gimbal_cmd", sizeof(Gimbal_Ctrl_Cmd_s));
    DJIMotorStop(yaw_motor);
    DJIMotorStop(pitch_motor);
}

/* 机器人云台控制核心任务,后续考虑只保留IMU控制,不再需要电机的反馈 */
void GimbalTask()
{
    AlarmSetStatus(buzzer, ALARM_OFF);
    // 获取云台控制数据
    // 后续增加未收到数据的处理
    SubGetMessage(gimbal_sub, &gimbal_cmd_recv);

// @todo:现在已不再需要电机反馈,实际上可以始终使用IMU的姿态数据来作为云台的反馈,yaw电机的offset只是用来跟随底盘
// 根据控制模式进行电机反馈切换和过渡,视觉模式在robot_cmd模块就已经设置好,gimbal只看yaw_ref和pitch_ref
#ifdef MAIN_BOARD
    switch (gimbal_cmd_recv.gimbal_mode)
    {
    // 停止
    case GIMBAL_ZERO_FORCE:
        // DJIMotorStop(yaw_motor);
        // DJIMotorStop(pitch_motor);
        DMMotorStop(chassis_yaw_motor);
        break;
    // 使用陀螺仪的反馈,底盘根据yaw电机的offset跟随云台或视觉模式采用
    case GIMBAL_GYRO_MODE: // 后续只保留此模式
        // DJIMotorEnable(yaw_motor);
        // DJIMotorEnable(pitch_motor);
        // DJIMotorChangeFeed(yaw_motor, ANGLE_LOOP, OTHER_FEED);
        // DJIMotorChangeFeed(yaw_motor, SPEED_LOOP, OTHER_FEED);
        // DJIMotorChangeFeed(pitch_motor, ANGLE_LOOP, OTHER_FEED);
        // DJIMotorChangeFeed(pitch_motor, SPEED_LOOP, OTHER_FEED);
        // DJIMotorSetRef(yaw_motor, gimbal_cmd_recv.yaw); // yaw和pitch会在robot_cmd中处理好多圈和单圈
        // DJIMotorSetRef(pitch_motor, gimbal_cmd_recv.pitch);
        DMMotorEnable(chassis_yaw_motor);
        DMMotorOuterLoop(chassis_yaw_motor, ANGLE_LOOP);
        DMMotorSetRef(chassis_yaw_motor, gimbal_cmd_recv.yaw);
        break;
    // 云台自由模式,使用编码器反馈,底盘和云台分离,仅云台旋转,一般用于调整云台姿态(英雄吊射等)/能量机关
    case GIMBAL_FREE_MODE: // 后续删除,或加入云台追地盘的跟随模式(响应速度更快)
                           // DJIMotorEnable(yaw_motor);
                           // DJIMotorEnable(pitch_motor);
                           // DJIMotorChangeFeed(yaw_motor, ANGLE_LOOP, OTHER_FEED);
                           // DJIMotorChangeFeed(yaw_motor, SPEED_LOOP, OTHER_FEED);
                           // DJIMotorChangeFeed(pitch_motor, ANGLE_LOOP, OTHER_FEED);
                           // DJIMotorChangeFeed(pitch_motor, SPEED_LOOP, OTHER_FEED);
                           // DJIMotorSetRef(yaw_motor, gimbal_cmd_recv.yaw); // yaw和pitch会在robot_cmd中处理好多圈和单圈
                           // DJIMotorSetRef(pitch_motor, gimbal_cmd_recv.pitch);
        // 此模式用于测试雷达的云台控制
        // DMMotorStop(chassis_yaw_motor);
        DMMotorEnable(chassis_yaw_motor);
        DMMotorOuterLoop(chassis_yaw_motor, ANGLE_LOOP);
        DMMotorSetRef(chassis_yaw_motor, gimbal_cmd_recv.yaw);
        break;
    default:
        break;
    }
    gimbal_feedback_data.yaw_motor_single_round_angle = chassis_yaw_motor->measure.angle_single_round;
#endif

    // 在合适的地方添加pitch重力补偿前馈力矩
    // 根据IMU姿态/pitch电机角度反馈计算出当前配重下的重力矩
    // ...

    // 设置反馈数据,主要是imu和yaw的ecd
    // gimbal_feedback_data.gimbal_imu_data = *gimba_IMU_data;
#if defined(GIMBAL_BOARD_LEFT) || defined(GIMBAL_BOARD_RIGHT)
    switch (gimbal_cmd_recv.gimbal_mode)
    {
    // 停止
    case GIMBAL_ZERO_FORCE:
        DJIMotorStop(yaw_motor);
        DJIMotorStop(pitch_motor);
        break;
    // 使用陀螺仪的反馈,底盘根据yaw电机的offset跟随云台或视觉模式采用
    case GIMBAL_GYRO_MODE: // 后续只保留此模式
        // DJIMotorStop(yaw_motor);
        // DJIMotorStop(pitch_motor);
        DJIMotorEnable(yaw_motor);
        DJIMotorEnable(pitch_motor);
        DJIMotorChangeFeed(yaw_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(yaw_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorSetRef(yaw_motor, gimbal_cmd_recv.yaw); // yaw和pitch会在robot_cmd中处理好多圈和单圈
        DJIMotorSetRef(pitch_motor, gimbal_cmd_recv.pitch);
        // DMMotorEnable(chassis_yaw_motor);
        // DMMotorOuterLoop(chassis_yaw_motor, SPEED_LOOP);
        // DMMotorSetRef(chassis_yaw_motor, gimbal_cmd_recv.yaw);
        break;
    // 云台自由模式,使用编码器反馈,底盘和云台分离,仅云台旋转,一般用于调整云台姿态(英雄吊射等)/能量机关
    case GIMBAL_FREE_MODE: // 后续删除,或加入云台追地盘的跟随模式(响应速度更快)
        DJIMotorEnable(yaw_motor);
        DJIMotorEnable(pitch_motor);
        DJIMotorChangeFeed(yaw_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(yaw_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorSetRef(yaw_motor, gimbal_cmd_recv.yaw); // yaw和pitch会在robot_cmd中处理好多圈和单圈
        DJIMotorSetRef(pitch_motor, gimbal_cmd_recv.pitch);
        // 此模式用于测试雷达的云台控制
        break;
    case GIMBAL_AIM_MODE:
        DJIMotorEnable(yaw_motor);
        DJIMotorEnable(pitch_motor);
        DJIMotorChangeFeed(yaw_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(yaw_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, ANGLE_LOOP, OTHER_FEED);
        DJIMotorChangeFeed(pitch_motor, SPEED_LOOP, OTHER_FEED);
        DJIMotorSetRef(yaw_motor, gimbal_cmd_recv.yaw); // yaw和pitch会在robot_cmd中处理好多圈和单圈
        DJIMotorSetRef(pitch_motor, gimbal_cmd_recv.pitch);
        break;
    default:
        break;
    }
    yaw_angle_ff = 21.715 * yaw_motor->measure.total_angle - 2893.6f;
    gimbal_feedback_data.yaw_motor_single_round_angle = yaw_motor->measure.total_angle;
#endif
    gimbal_feedback_data.gimbal_imu_data = *gimba_IMU_data;
    // 推送消息
    PubPushMessage(gimbal_pub, (void *)&gimbal_feedback_data);
}