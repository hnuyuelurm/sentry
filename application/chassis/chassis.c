/**
 * @file chassis.c
 * @author NeoZeng neozng1@hnu.edu.cn
 * @brief 底盘应用,负责接收robot_cmd的控制命令并根据命令进行运动学解算,得到输出
 *        注意底盘采取右手系,对于平面视图,底盘纵向运动的正前方为x正方向;横向运动的右侧为y正方向
 *
 * @version 0.1
 * @date 2022-12-04
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "chassis.h"
#include "robot_def.h"
#include "dji_motor.h"
#include "super_cap.h"
#include "message_center.h"
#include "referee_task.h"

#include "general_def.h"
#include "bsp_dwt.h"
#include "referee_UI.h"
#include "arm_math.h"

/* 根据robot_def.h中的macro自动计算的参数 */
#define HALF_WHEEL_BASE (WHEEL_BASE / 2.0f)     // 半轴距
#define HALF_TRACK_WIDTH (TRACK_WIDTH / 2.0f)   // 半轮距
#define PERIMETER_WHEEL (RADIUS_WHEEL * 2 * PI) // 轮子周长
#define sqrt_2 1.41421356237f                   // 根号2

/* 底盘应用包含的模块和信息存储,底盘是单例模式,因此不需要为底盘建立单独的结构体 */
#ifdef CHASSIS_BOARD // 如果是底盘板,使用板载IMU获取底盘转动角速度
#include "can_comm.h"
#include "ins_task.h"
static CANCommInstance *chasiss_can_comm; // 双板通信CAN comm
attitude_t *Chassis_IMU_data;
#endif // CHASSIS_BOARD
#if defined(ONE_BOARD) || defined(MAIN_BOARD)
static Publisher_t *chassis_pub;                    // 用于发布底盘的数据
static Subscriber_t *chassis_sub;                   // 用于订阅底盘的控制命令
#endif                                              // !ONE_BOARD
static Chassis_Ctrl_Cmd_s chassis_cmd_recv;         // 底盘接收到的控制命令
static Chassis_Upload_Data_s chassis_feedback_data; // 底盘回传的反馈数据

static attitude_t *chassis_IMU_data;
static referee_info_t *referee_data;       // 用于获取裁判系统的数据
static Referee_Interactive_info_t ui_data; // UI数据，将底盘中的数据传入此结构体的对应变量中，UI会自动检测是否变化，对应显示UI
static float power_data[2];
static SuperCap_Msg_s supercap_recv_data;
static SuperCapInstance *cap;                                       // 超级电容
static DJIMotorInstance *motor_lf, *motor_rf, *motor_lb, *motor_rb; // left right forward back
static float offsetradians;
static float bullet_speed_l, bullet_speed_r;
static float bullet_freq_l, bullet_freq_r;
static float chassis_power;
/* 用于自旋变速策略的时间变量 */
// static float t;

/* 私有函数计算的中介变量,设为静态避免参数传递的开销 */
static float chassis_vx, chassis_vy;     // 将云台系的速度投影到底盘
static float vt_lf, vt_rf, vt_lb, vt_rb; // 底盘速度解算后的临时输出,待进行限幅
static float sin_theta, cos_theta;
static float vx, vy, wz;
void ChassisInit()
{
    // chassis_IMU_data = INS_Init();
    //  四个轮子的参数一样,改tx_id和反转标志位即可
    Motor_Init_Config_s chassis_motor_config = {
        .can_init_config.can_handle = &hcan1,
        .controller_param_init_config = {
            .speed_PID = {
                .Kp = 9, // 4.5
                .Ki = 0, // 0
                .Kd = 0, // 0
                .IntegralLimit = 3000,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement | PID_OutputFilter,
                .Output_LPF_RC = 0.07,
                .MaxOut = 12000,
            },
            .current_PID = {
                .Kp = 0.55, // 0.4
                .Ki = 0,    // 0
                .Kd = 0,
                .IntegralLimit = 3000,
                .Improve = PID_Trapezoid_Intergral | PID_Integral_Limit | PID_Derivative_On_Measurement,
                .MaxOut = 15000,
            },
        },
        .controller_setting_init_config = {
            .angle_feedback_source = MOTOR_FEED,
            .speed_feedback_source = MOTOR_FEED,
            .outer_loop_type = SPEED_LOOP,
            .close_loop_type = SPEED_LOOP | CURRENT_LOOP,
        },
        .motor_type = M3508,
    };
    //  @todo: 当前还没有设置电机的正反转,仍然需要手动添加reference的正负号,需要电机module的支持,待修改.
    chassis_motor_config.can_init_config.tx_id = 1;
    chassis_motor_config.controller_setting_init_config.motor_reverse_flag = MOTOR_DIRECTION_NORMAL;
    motor_lf = DJIMotorInit(&chassis_motor_config);

    chassis_motor_config.can_init_config.tx_id = 2;
    chassis_motor_config.controller_setting_init_config.motor_reverse_flag = MOTOR_DIRECTION_REVERSE;
    motor_rf = DJIMotorInit(&chassis_motor_config);

    chassis_motor_config.can_init_config.tx_id = 4;
    chassis_motor_config.controller_setting_init_config.motor_reverse_flag = MOTOR_DIRECTION_NORMAL;
    motor_lb = DJIMotorInit(&chassis_motor_config);

    chassis_motor_config.can_init_config.tx_id = 3;
    chassis_motor_config.controller_setting_init_config.motor_reverse_flag = MOTOR_DIRECTION_REVERSE;
    motor_rb = DJIMotorInit(&chassis_motor_config);

    // referee_data = UITaskInit(&huart6, &ui_data); // 裁判系统初始化,会同时初始化UI
    referee_data = RefereeInit(&huart6); // 初始化裁判系统的串口,并返回裁判系统反馈数据指针
    SuperCap_Init_Config_s cap_conf = {
        .can_config = {
            .can_handle = &hcan1,
            .tx_id = 0x302, // 超级电容默认接收id
            .rx_id = 0x301, // 超级电容默认发送id,注意tx和rx在其他人看来是反的
        }};
    cap = SuperCapInit(&cap_conf); // 超级电容初始化

    // 发布订阅初始化,如果为双板,则需要can comm来传递消息
#ifdef CHASSIS_BOARD
    Chassis_IMU_data = INS_Init(); // 底盘IMU初始化

    CANComm_Init_Config_s comm_conf = {
        .can_config = {
            .can_handle = &hcan2,
            .tx_id = 0x311,
            .rx_id = 0x312,
        },
        .recv_data_len = sizeof(Chassis_Ctrl_Cmd_s),
        .send_data_len = sizeof(Chassis_Upload_Data_s),
    };
    chasiss_can_comm = CANCommInit(&comm_conf); // can comm初始化
#endif                                          // CHASSIS_BOARD

#if defined(ONE_BOARD) || defined(MAIN_BOARD) // 单板控制整车,则通过pubsub来传递消息
    chassis_sub = SubRegister("chassis_cmd", sizeof(Chassis_Ctrl_Cmd_s));
    chassis_pub = PubRegister("chassis_feed", sizeof(Chassis_Upload_Data_s));
#endif // ONE_BOARD
}

#define LF_CENTER ((HALF_TRACK_WIDTH + CENTER_GIMBAL_OFFSET_X + HALF_WHEEL_BASE - CENTER_GIMBAL_OFFSET_Y) * DEGREE_2_RAD)
#define RF_CENTER ((HALF_TRACK_WIDTH - CENTER_GIMBAL_OFFSET_X + HALF_WHEEL_BASE - CENTER_GIMBAL_OFFSET_Y) * DEGREE_2_RAD)
#define LB_CENTER ((HALF_TRACK_WIDTH + CENTER_GIMBAL_OFFSET_X + HALF_WHEEL_BASE + CENTER_GIMBAL_OFFSET_Y) * DEGREE_2_RAD)
#define RB_CENTER ((HALF_TRACK_WIDTH - CENTER_GIMBAL_OFFSET_X + HALF_WHEEL_BASE + CENTER_GIMBAL_OFFSET_Y) * DEGREE_2_RAD)
/**
 * @brief 计算每个轮毂电机的输出,正运动学解算
 *        用宏进行预替换减小开销,运动解算具体过程参考教程
 */
static void MecanumCalculate()
{
    vt_lf = (chassis_vx - chassis_vy) * sqrt_2 - chassis_cmd_recv.wz * LF_CENTER;
    vt_rf = (chassis_vx + chassis_vy) * sqrt_2 + chassis_cmd_recv.wz * RF_CENTER;
    vt_lb = (chassis_vx + chassis_vy) * sqrt_2 - chassis_cmd_recv.wz * LB_CENTER;
    vt_rb = (chassis_vx - chassis_vy) * sqrt_2 + chassis_cmd_recv.wz * RB_CENTER;
}

/**
 * @brief 根据裁判系统和电容剩余容量对输出进行限制并设置电机参考值
 *
 */
static void LimitChassisOutput()
{
    // 功率限制待添加
    power_data[0] = referee_data->PowerHeatData.buffer_energy;
    power_data[1] = 100.0f;
    SuperCapSend(cap, (uint8_t *)&power_data);
    // 完成功率限制后进行电机参考输入设定
    DJIMotorSetRef(motor_lf, vt_lf);
    DJIMotorSetRef(motor_rf, vt_rf);
    DJIMotorSetRef(motor_lb, vt_lb);
    DJIMotorSetRef(motor_rb, vt_rb);
}

/**
 * @brief 根据每个轮子的速度反馈,计算底盘的实际运动速度,逆运动解算
 *        对于双板的情况,考虑增加来自底盘板IMU的数据
 *
 */
static void EstimateSpeed()
{
    // 根据电机速度和陀螺仪的角速度进行解算,还可以利用加速度计判断是否打滑(如果有)
    // 速度反馈为云台系
    // vy = (motor_lf->measure.speed_aps + motor_rf->measure.speed_aps - motor_lb->measure.speed_aps - motor_rb->measure.speed_aps) / 4.0f / REDUCTION_RATIO_WHEEL / 360.0f * PERIMETER_WHEEL / 1000.0f;
    vx = (motor_lf->measure.speed_aps - motor_rf->measure.speed_aps + motor_lb->measure.speed_aps - motor_rb->measure.speed_aps) / 4.0f / REDUCTION_RATIO_WHEEL / 360.0f * PERIMETER_WHEEL / 1000.0f / sqrt_2;
    vy = (-motor_lf->measure.speed_aps - motor_rf->measure.speed_aps + motor_lb->measure.speed_aps + motor_rb->measure.speed_aps) / 4.0f / REDUCTION_RATIO_WHEEL / 360.0f * PERIMETER_WHEEL / 1000.0f / sqrt_2;
    wz = (-motor_lf->measure.speed_aps - motor_rf->measure.speed_aps - motor_lb->measure.speed_aps - motor_rb->measure.speed_aps) / (LF_CENTER + RF_CENTER + LB_CENTER + RB_CENTER);
    chassis_feedback_data.real_vx = vx * cos_theta + vy * sin_theta;
    chassis_feedback_data.real_vy = -vx * sin_theta + vy * cos_theta;
}

/* 机器人底盘控制核心任务 */
void ChassisTask()
{
    // 后续增加没收到消息的处理(双板的情况)
    // 获取新的控制信息
#if defined(ONE_BOARD) || defined(MAIN_BOARD)
    SubGetMessage(chassis_sub, &chassis_cmd_recv);
#endif
#ifdef CHASSIS_BOARD
    chassis_cmd_recv = *(Chassis_Ctrl_Cmd_s *)CANCommGet(chasiss_can_comm);
#endif // CHASSIS_BOARD
    supercap_recv_data = SuperCapGet(cap);
    if (chassis_cmd_recv.chassis_mode == CHASSIS_ZERO_FORCE)
    { // 如果出现重要模块离线或遥控器设置为急停,让电机停止
        DJIMotorStop(motor_lf);
        DJIMotorStop(motor_rf);
        DJIMotorStop(motor_lb);
        DJIMotorStop(motor_rb);
    }
    else
    { // 正常工作
        DJIMotorEnable(motor_lf);
        DJIMotorEnable(motor_rf);
        DJIMotorEnable(motor_lb);
        DJIMotorEnable(motor_rb);
    }

    // 根据控制模式设定旋转速度
    switch (chassis_cmd_recv.chassis_mode)
    {
    case CHASSIS_NO_FOLLOW: // 底盘不旋转,但维持全向机动,一般用于调整云台姿态
        chassis_cmd_recv.wz = 0.0f;
        break;
    case CHASSIS_FOLLOW_GIMBAL_YAW: // 跟随云台,不单独设置pid,以误差角度平方为速度输出
        chassis_cmd_recv.wz = 1.5f * chassis_cmd_recv.offset_angle * abs(chassis_cmd_recv.offset_angle);
        break;
    case CHASSIS_ROTATE: // 自旋,同时保持全向机动;当前wz维持定值,后续增加不规则的变速策略
        chassis_cmd_recv.wz = 2200.0f;
        break;
    default:
        break;
    }

    // 根据云台和底盘的角度offset将控制量映射到底盘坐标系上
    // 底盘逆时针旋转为角度正方向;云台命令的方向以云台指向的方向为x,采用右手系(x指向正北时y在正西)
    offsetradians = chassis_cmd_recv.offset_angle * DEGREE_2_RAD;
    cos_theta = arm_cos_f32(offsetradians);
    sin_theta = arm_sin_f32(offsetradians);
    chassis_vx = chassis_cmd_recv.vx * cos_theta - chassis_cmd_recv.vy * sin_theta;
    chassis_vy = chassis_cmd_recv.vx * sin_theta + chassis_cmd_recv.vy * cos_theta;
    // chassis_vx = chassis_cmd_recv.vx;
    // chassis_vy = chassis_cmd_recv.vy;
    // 根据控制模式进行正运动学解算,计算底盘输出
    MecanumCalculate();

    // 根据裁判系统的反馈数据和电容数据对输出限幅并设定闭环参考值
    LimitChassisOutput();

    // 根据电机的反馈速度和IMU(如果有)计算真实速度
    EstimateSpeed();

    // // 获取裁判系统数据   建议将裁判系统与底盘分离，所以此处数据应使用消息中心发送
    // // 我方颜色id小于等于7是红色,大于7是蓝色,注意这里发送的是对方的颜色, 2:blue , 1:red
    // chassis_feedback_data.enemy_color = referee_data->GameRobotState.robot_id > 7 ? 2 : 1; // 其实是己方颜色

    // // 当前只做了17mm热量的数据获取,后续根据robot_def中的宏切换双枪管和英雄42mm的情况
    chassis_feedback_data.bullet_speed = referee_data->GameRobotState.shooter_barrel_heat_limit;
    if (referee_data->ShootData.shooter_id == 1)
    {
        bullet_speed_l = referee_data->ShootData.bullet_speed;
        bullet_freq_l = referee_data->ShootData.bullet_freq;
    }
    else
    {
        bullet_speed_r = referee_data->ShootData.bullet_speed;
        bullet_freq_r = referee_data->ShootData.bullet_freq;
    }

    // chassis_feedback_data.gamestate = referee_data->GameState.game_progress;
    // chassis_feedback_data.rest_heat = referee_data->PowerHeatData.shooter_heat0;
    // chassis_feedback_data.real_vx=chassis_IMU_data->Gyro[0];
    SentryCmdSend();
    chassis_feedback_data.rest_heat_l = (uint16_t)(400 - referee_data->PowerHeatData.shooter_17mm_1_barrel_heat);
    chassis_feedback_data.rest_heat_r = (uint16_t)(400 - referee_data->PowerHeatData.shooter_17mm_2_barrel_heat);
    chassis_feedback_data.game_progress = (float)referee_data->GameState.game_progress;
    chassis_feedback_data.stage_remain_time = referee_data->GameState.game_progress == 4 ? (float)referee_data->GameState.stage_remain_time : 0;
    chassis_feedback_data.current_HP = (float)referee_data->GameRobotState.current_HP;
#ifdef RED
    chassis_feedback_data.enemy_color = 1;
    chassis_feedback_data.current_base_hp = (float)referee_data->GameRobotHP.red_base_HP;              // 需要根据己方颜色设定己方基地血量
    chassis_feedback_data.current_enemy_base_hp = (float)referee_data->GameRobotHP.blue_base_HP;       // 同上
    chassis_feedback_data.current_enemy_sentry_hp = (float)referee_data->GameRobotHP.blue_7_robot_HP;  // 同上，敌方哨兵血量
    chassis_feedback_data.current_shield_hp = 100.0f;                                                  // 盾牌血量
    chassis_feedback_data.current_outpost_hp = (float)referee_data->GameRobotHP.red_outpost_HP;        // 前哨站血量
    chassis_feedback_data.current_enemy_outpost_hp = (float)referee_data->GameRobotHP.blue_outpost_HP; // 敌方前哨站血量
#endif
#ifdef BLUE
    chassis_feedback_data.enemy_color = 2;
    chassis_feedback_data.current_base_hp = (float)referee_data->GameRobotHP.blue_base_HP;
    chassis_feedback_data.current_enemy_base_hp = (float)referee_data->GameRobotHP.red_base_HP;
    chassis_feedback_data.current_enemy_sentry_hp = (float)referee_data->GameRobotHP.red_7_robot_HP;
    chassis_feedback_data.current_shield_hp = 100.0f;
    chassis_feedback_data.current_outpost_hp = (float)referee_data->GameRobotHP.blue_outpost_HP;
    chassis_feedback_data.current_enemy_outpost_hp = (float)referee_data->GameRobotHP.red_outpost_HP;
#endif
    chassis_feedback_data.allow_fire_amount = (float)referee_data->PojectileAllowance.projectile_allowance_17mm; // 允许发射的子弹数量
    chassis_feedback_data.cmd_x = referee_data->MapCommand.target_position_x;
    chassis_feedback_data.cmd_y = referee_data->MapCommand.target_position_y;
    chassis_feedback_data.cmd_enable = 0;
    chassis_power = referee_data->PowerHeatData.chassis_voltage / 1000.0f * referee_data->PowerHeatData.chassis_current / 1000.0f;
    // 推送反馈消息
#if defined(ONE_BOARD) || defined(MAIN_BOARD)
    PubPushMessage(chassis_pub, (void *)&chassis_feedback_data);
#endif
#ifdef CHASSIS_BOARD
    CANCommSend(chasiss_can_comm, (void *)&chassis_feedback_data);
#endif // CHASSIS_BOARD
}