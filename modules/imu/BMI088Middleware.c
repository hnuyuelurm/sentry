#include "BMI088Middleware.h"
#include "main.h"

SPI_HandleTypeDef *BMI088_SPI[2];

void BMI088_ACCEL_NS_L(uint8_t idx)
{
    if(idx == 0){
        HAL_GPIO_WritePin(CS1_ACCEL_GPIO_Port, CS1_ACCEL_Pin, GPIO_PIN_RESET);
    }else{
        HAL_GPIO_WritePin(CS2_ACCEL_GPIO_Port, CS2_ACCEL_Pin, GPIO_PIN_RESET);
    }
}
void BMI088_ACCEL_NS_H(uint8_t idx)
{
    if(idx == 0){
        HAL_GPIO_WritePin(CS1_ACCEL_GPIO_Port, CS1_ACCEL_Pin, GPIO_PIN_SET);
    }else{
        HAL_GPIO_WritePin(CS2_ACCEL_GPIO_Port, CS2_ACCEL_Pin, GPIO_PIN_SET);
    }
}


void BMI088_GYRO_NS_L(uint8_t idx)
{
    if(idx == 0){
        HAL_GPIO_WritePin(CS1_GYRO_GPIO_Port, CS1_GYRO_Pin, GPIO_PIN_RESET);
    }else{
        HAL_GPIO_WritePin(CS2_GYRO_GPIO_Port, CS2_GYRO_Pin, GPIO_PIN_RESET);
    }
}

void BMI088_GYRO_NS_H(uint8_t idx)
{
    if(idx == 0){
        HAL_GPIO_WritePin(CS1_GYRO_GPIO_Port, CS1_GYRO_Pin, GPIO_PIN_SET);
    }else{
        HAL_GPIO_WritePin(CS2_GYRO_GPIO_Port, CS2_GYRO_Pin, GPIO_PIN_SET);
    }
}


uint8_t BMI088_read_write_byte(uint8_t txdata, uint8_t idx)
{
    uint8_t rx_data;
    HAL_SPI_TransmitReceive(BMI088_SPI[idx], &txdata, &rx_data, 1, 1000);
    return rx_data;
}
