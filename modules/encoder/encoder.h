#ifndef ENCODER_H
#define ENCODER_H

#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"
#include "stdlib.h"

typedef struct
{
    TIM_HandleTypeDef *htim;
    uint32_t RiseCount;
    uint32_t FallCount;
    uint32_t period;
    float duty;
    float last_duty;
    float angle_single_round;
    float total_angle;
    int32_t total_round;
} EncoderInstance;

typedef struct
{
    TIM_HandleTypeDef *htim;
    uint32_t count;
    uint32_t period;
} EncoderConfig_s;

EncoderInstance *EncoderRegister(EncoderConfig_s *config);
void EncoderDecode(EncoderInstance *instance);
#endif